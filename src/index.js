const fs = require('fs');

function writeToFile( filePath, data ) {
	fs.appendFile(filePath, data, (err) => {
		if (err) throw err;
	});
}

let data = process.argv,
	defaultArgsQty = 2, // As I understand the first 2 options of process.argv always contain non args.
	filename = data[defaultArgsQty]; //The first argument is filename

data = data.slice( defaultArgsQty + 1 ); //The rest of arguments need to be written to the file
data = data.join(" ");

writeToFile( filename, data );